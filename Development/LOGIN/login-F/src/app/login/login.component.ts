import { User } from './../user/user';
import { UserserviceService } from './../service/userservice.service';
import { Component, OnInit, resolveForwardRef } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { bootstrapApplication } from '@angular/platform-browser';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  /*
  loginToken = new FormGroup({
    user: new FormControl(),
    password: new FormControl()
  });

  Username!: string;
  password!: string;
  message!: string;
  alert!: String;
  showAlert = false;
  userLogin: UserserviceService = {} as UserserviceService;

  */

  model: any = {}
  getData!: boolean;



  constructor(
    private Userservice: UserserviceService,
    private router:Router,

    ){}

  ngOnInit(){
  }



  loginUser(){
    const user = this.model.username;
    const password = this.model.password;

    this.Userservice.getUserData(user, password).subscribe((res:any) => {
      this.getData = res;

      if (this.getData == true) {
        this.router.navigate(['/home']);
      }
    })
  }




/*
OnLoginToken (): void {
  console.log(this.loginToken.value.user);
  console.log(this.loginToken.value.password);

  this.Userservice.getUserData = this.loginToken.value.user;
  this.Userservice.getUserData = this.loginToken.value.password;

  if(this.password !== this.password) {
    this.alert = "alert alert-danger", this.message ='la password non coincide', this.showAlert = true,
    console.log(this.alert);
  } else {
    this.Userservice.getUserData(this.Username, this.password).subscribe({



    })


  }
  return;
}

*/

}
