package com.geston.productos.controller;


import com.geston.productos.entity.Producto;
import com.geston.productos.service.Productoservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class ProductoController {

    @Autowired
    private Productoservice productoservice;

    @RequestMapping("/")
    public String verPaginaDeInicio(Model model, @Param("palabraClave") String palabraClave){
        List<Producto> listaProductos = productoservice.listAll(palabraClave);

        model.addAttribute("listaProductos", listaProductos);
        model.addAttribute("palabraClave", palabraClave);
        return "index";
    }

    @RequestMapping("nuevo")
    public String mostrarFormularioNuevoProducto(Model model){
        Producto producto = new Producto();
        model.addAttribute("producto", producto);
        return "nuevo_producto";
    }


    @RequestMapping(value = "/guardar", method = RequestMethod.POST)
    public String guardarProducto(@ModelAttribute("producto") Producto producto){
       productoservice.save(producto);
       return "redirect:/";
    }

    @RequestMapping("/editar/{id}")
    public ModelAndView mostrarFormularioDeEditarProducto(@PathVariable(name = "id") int id){
        ModelAndView modelo = new ModelAndView("editar_producto");

        Producto producto = productoservice.get(id);
        modelo.addObject("producto", producto);

        return modelo;
    }

    @RequestMapping("/eliminar/{id}")
    public String eliminarProducto (@PathVariable(name = "id")int id){
        productoservice.delete(id);
        return "redirect:/";
    }

}
