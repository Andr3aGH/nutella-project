package com.geston.productos.service;


import com.geston.productos.entity.Producto;
import com.geston.productos.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Productoservice {

    @Autowired
    private ProductoRepository productoRepository;

    public List<Producto> listAll(String palabraClave){
        if (palabraClave != null){
            return productoRepository.findAll(palabraClave);
        }
        return productoRepository.findAll();
    }

    public void save(Producto producto){
        productoRepository.save(producto);
    }

    public Producto get(int id){
        return productoRepository.findById(id).get();
    }

    public void delete(int id){
        productoRepository.deleteById(id);
    }
}
